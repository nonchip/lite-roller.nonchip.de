--DB = require 'harmonia.db'
--DB.connect!

RegexRouter = require 'dysnomia.routers.regex_router'
config = require 'dysnomia.config'

with RegexRouter!
  \add '/ws', require 'apps.socket'
  \addNested '', require 'apps.web'

  \add '__404', (matches,data)->
    ngx.status = 404
    ngx.say '404 not found: '..data.uri
    if config.app.debug
      ngx.say '<code><pre>'
      ngx.say debug.traceback!
      ngx.say '</pre></code>'
    ngx.exit(ngx.OK)
  \add '__500', (matches,data)->
    ngx.status = 500
    ngx.say '<code><pre>'
    ngx.say data
    ngx.say '</pre></code>'
    ngx.exit(ngx.OK)
  \dispatch!
