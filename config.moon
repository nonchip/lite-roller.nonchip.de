->
  app ->
    debug true
    skillnames ->
      alpha 'Skill 1'
      beta 'Skill 2'
      gamma 'Skill 3'
      delta 'Skill 4'
  ngx ->
    debug true -- turn this off in production
    daemon false -- turn this on in production
    worker_processes 1 -- up this value in production, 8 and above works fine for me
    events ->
      worker_connections 10 -- up this value in production, 64 and above works fine for me, if you get http errors or websocket connection issues, up it more
    http ->
      server ->
        port 8080 -- set this to your port (both http+websocket)
        lua_code_cache false -- turn this on in production
        aliases _ARR{
          ->
            location '/static/'
            alias 'static/'
            --cache '1d' -- turn this on in production
        }
