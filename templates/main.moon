HDSL = require'dysnomia.renderers.html5_dsl'
config = require 'dysnomia.config'
RollResult = require'templates.roll_result'
CharListEntry = require'templates.char_list_entry'

HDSL =>
  HTML5 ->
    head ->
      meta charset:'utf-8'
      link rel:"stylesheet", href:"https://unpkg.com/purecss@1.0.0/build/pure-min.css", integrity:"sha384-nn4HPE8lTHyVtfCBi5yW9d20FjT8BJwUXyWZT9InLYax14RDjBj46LmSztkmNP9w", crossorigin:"anonymous"
      script src:"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"
      script src:"/static/reconnecting-websocket.min.js"
      script src:"/static/jsviews.min.js"
      link rel:"stylesheet", href:"/static/main.less.css"
      script src:"/static/main.coffee.js"
      meta name:"viewport", content:"width=device-width, initial-scale=1"
      RAW [[
        <!--[if lte IE 8]>
          <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/grids-responsive-old-ie-min.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
          <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/grids-responsive-min.css">
        <!--<![endif]-->
      ]]
      if @hooks and @hooks.head
        RAW @hooks.head @
    body ->
      script id: "tpl_roll", type: "text/x-jsrender", RollResult\render!
      script id: "tpl_char", type: "text/x-jsrender", CharListEntry\render!
      div id: "layout", class: "pure-g", ->
        div id: "toolbar", class: "pure-u-1", ->
          button type:'button', class: "pure-button pure-input-1-3", onclick: 'window.addChar()', 'Add Char'
          a style: 'margin-left: 5em', href: 'https://gitlab.com/nonchip/lite-roller.nonchip.de', 'Source Code'
        div id: "chars", class: "pure-u-2-3", ""
        div id: "rolls", class: "pure-u-1-3", ""
