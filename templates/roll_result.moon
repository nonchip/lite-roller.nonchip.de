HDSL = require'dysnomia.renderers.html5_dsl'
config = require 'dysnomia.config'

HDSL =>
  div class: "pure-g roll-result", ->
    div class: "pure-u-1", ['data-link']: 'text{:name()}', (@name or "")
    div class: "pure-u-1", ->
      button class: "pure-button rollresult-button rollresult-button-success", name:"success", ['data-link']: '{on ~useroll} text{:successes()}', (@successes or "")
      button class: "pure-button rollresult-button rollresult-button-failure", name:"failure", ['data-link']: '{on ~useroll} text{:failures()}', (@failures or "")
