HDSL = require'dysnomia.renderers.html5_dsl'
config = require 'dysnomia.config'

linkededitable = (field,grid1=1,grid2=nil,btn=true,fieldtext)->
  HDSL =>
    div class: "pure-u-"..grid1..(grid2 and '-'..grid2 or ''), ->
      fieldtext or= config.app.skillnames[field]
      RAW '{^{if editable()}}'
      input class: "pure-input-1", placeholder: fieldtext, title: fieldtext, ['data-link']: field..'() trigger=false', value: (@[field] or nil)
      RAW '{{else}}'
      if btn
        button type:'button', class: "pure-button pure-input-1", name: field, title: fieldtext, ['data-link']: '{on ~charroll} text{:'..field..'()}'
      else
        span name: field, title: fieldtext, ['data-link']: 'text{:'..field..'()}'
      RAW '{{/if}}'


HDSL =>
  form class: "pure-form pure-g char-list-entry", ->
    RAW linkededitable('name', 5, 12,false,"Name")\render @
    div class: "pure-u-1-2 pure-g", ->
      for i in *{'alpha','beta','gamma','delta'}
        RAW linkededitable(i, 1, 5)\render @
      div class: "pure-u-1-5", ->
        input class: "pure-input-2-3", placeholder: '±mod', title: '± modifier', ['data-link']: 'mod() trigger=false', value: (@mod or nil)
        button type:'button', class: "pure-button pure-input-1-3", style:'padding-left:0;padding-right:0', name: 'mod', title: 'Roll mod only', ['data-link']: '{on ~charroll}', '±'
    div class: "pure-u-1-12", ->
      input type: 'checkbox', class: "pure-checkbox pure-input-1-3", style:'display:inline-block', ['data-link']: 'editable()', title: "edit"
      RAW '{^{if editable()}}'
      button type:'button', class: "pure-button pure-input-1-4", style:'padding-left:0;padding-right:0', name: 'del', title: 'Delete', ['data-link']: '{on \'dblclick\' ~delchar}', '🗑'
      RAW '{{/if}}'
