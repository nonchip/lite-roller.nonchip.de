RegexRouter = require 'dysnomia.routers.regex_router'

MainTpl = require 'templates.main'
RollResultTpl = require 'templates.roll_result'

router = with RegexRouter!
  \add '/', (matches)->
    ngx.say MainTpl\render!
    ngx.exit(ngx.OK)

router
