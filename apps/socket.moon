server = require "resty.websocket.server"
cjson = require "cjson"
require "resty.core"

math.randomseed os.time!

getgd = (key)->
  j = ngx.shared.gamedata\get key
  return {} unless j
  return cjson.decode j

setgd = (key,val)->
  j = cjson.encode val
  ngx.shared.gamedata\set key,j

getid = ->
  ngx.shared.gamedata\incr 'id', 1, 0

handle_msg = (input)->
  if not input.cmd
    return {err: 'missing cmd'}
  switch input.cmd
    when 'make char'
      tbl = getgd 'chars'
      tbl or= {}
      table.insert tbl, {id: getid!}
      setgd 'chars', tbl
    when 'set char'
      tbl = getgd 'chars'
      tbl or= {}
      input.char.editable=nil
      input.char.mod=nil
      newtbl = {input.char}
      for r in *tbl
        if r.id != input.char.id
          table.insert newtbl, r
      setgd 'chars', newtbl
    when 'del char'
      tbl = getgd 'chars'
      tbl or= {}
      newtbl = {}
      for r in *tbl
        if r.id != input.id
          table.insert newtbl, r
      setgd 'chars', newtbl
    when 'roll'
      successes = math.random 0, input.amount
      failures = input.amount - successes
      tbl = getgd 'roll_results'
      tbl or= {}
      for c in *tbl
        if c.name == input.name
          return {err: 'already rolled'}
      table.insert tbl, {id: getid!, name:input.name, :successes, :failures}
      setgd 'roll_results', tbl
    when 'use'
      tbl = getgd 'roll_results'
      tbl or= {}
      newtbl = {}
      for r in *tbl
        if r.id == input.id
          if input.which == 'success'
            r.successes = math.max 0, r.successes-1
          else
            r.failures = math.max 0, r.failures-1
        if r.successes > 0 or r.failures > 0
          table.insert newtbl, r
      setgd 'roll_results', newtbl
  return gamedata: {
    roll_results: getgd 'roll_results'
    chars: getgd 'chars'
  }

->
  wb, err = server\new{
    timeout: 5000
    max_payload_len: 65535
  }

  if not wb
    ngx.log ngx.ERR, "failed to new websocket: ", err
    return ngx.exit 444
  
  strikes=0
  while strikes < 5

    data, typ, err = wb\recv_frame!
    if not data
      if not string.find(err, "timeout", 1, true)
        ngx.log ngx.ERR, "failed to receive a frame: ", err
      strikes += 1
      continue

    switch typ
      when "close"
        -- for typ "close", err contains the status code
        code = err
        -- send a close frame back:
        bytes, err = wb\send_close 1000, "enough, enough!"
        if not bytes
          ngx.log ngx.ERR, "failed to send the close frame: ", err
          return
        ngx.log ngx.INFO, "closing with status code ", code, " and message ", data
        return
      when "ping"
        -- send a pong frame back:
        bytes, err = wb\send_pong data
        if bytes
          strikes = 0
        else
          ngx.log ngx.ERR, "failed to send frame: ", err
          strikes += 1
          continue
      when "pong"
        continue -- ignore pongs
      else
        tbl = cjson.decode data
        if not tbl
          ngx.log ngx.ERR, "failed to parse table from: ", data
        bytes, err = wb\send_text cjson.encode handle_msg tbl
        if not bytes
          ngx.log ngx.ERR, "failed to send a text frame: ", err
          strikes +=1
    strikes = 0